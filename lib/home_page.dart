import 'package:flutter/material.dart';


class HomePage extends StatelessWidget {

	static String tag = 'home-page';

	@override
	Widget build(BuildContext context) {

		final buocard = Hero(
			tag: 'hero',
			child: Padding(
				padding: EdgeInsets.all(16.0),
				child: CircleAvatar(
					radius: 72.0,
					backgroundColor: Colors.transparent,
					backgroundImage: AssetImage('assets/images/usuario_6.jpg'),
				),
			),
		);

		final welcome = Padding(
			padding: EdgeInsets.all(8.0),
			child: Text(
				'Bienvenid@s',
				style: TextStyle(
					fontSize: 28.0,
					color: Colors.white,
				),
			),
		);

		final lorem = Padding(
			padding: EdgeInsets.all(8.0),
			child: Text(
				'Occaecat cupidatat anim aliqua consectetur deserunt proident dolor officia dolor tempor velit deserunt sit id officia magna sunt mollit nisi ut do in aliquip commodo ut incididunt.',
				style: TextStyle(
					fontSize: 16.0,
					color: Colors.white,
				),
			),
		);

		final cuerpo = Container(
			width: MediaQuery.of(context).size.width,
			padding: EdgeInsets.all(28.0),
			decoration: BoxDecoration(
				gradient: LinearGradient(
					colors: <Color>[Colors.blue, Colors.lightBlueAccent],
				),
			),
			child: Column(children: <Widget>[
				buocard, welcome, lorem
			]),
		);

		return Scaffold(
			body: cuerpo,
		);
		
	}
}