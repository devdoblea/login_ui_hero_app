import 'package:flutter/material.dart';
import 'package:login_ui_hero_app/home_page.dart';


class LoginPage extends StatefulWidget {

  static String tag = 'login-page';

  @override
  _LoginPageState createState() => _LoginPageState();
}


class _LoginPageState extends State<LoginPage> {

	@override
  Widget build(BuildContext context) {

		final logo = Hero(
			tag: 'hero',
			child: CircleAvatar(
				backgroundColor: Colors.transparent,
				radius: 48.0,
				child: Image.asset('assets/images/buo-logo.png'),
			),
		);

		final email = TextFormField(
			//onChanged: (s) => bloc.passwordChanged.add(s),
			//obscureText: true,
	    keyboardType: TextInputType.emailAddress,
	    autofocus: false,
	    initialValue: 'prueba@gmail.com',
	    decoration: InputDecoration(
	    	hintText: "Escribe tu Email",
	    	contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
	      border: OutlineInputBorder(
	      	borderRadius: BorderRadius.circular(32.0),
	      ),
	      labelText: "Email",
	      //errorText: snapshot.error,
	    ),
	  );

	  final password = TextFormField(
			//onChanged: (s) => bloc.passwordChanged.add(s),
			obscureText: true,
	    keyboardType: TextInputType.visiblePassword,
	    autofocus: false,
	    decoration: InputDecoration(
	    	hintText: "Escribe tu Password",
	    	contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
	      border: OutlineInputBorder(
	      	borderRadius: BorderRadius.circular(32.0),
	      ),
	      labelText: "Password",
	      //errorText: snapshot.error,
	    ),
	  );

	  final loginButton = Padding(
	  	padding: EdgeInsets.symmetric(vertical: 16.0),
	  	child: Material(
	  		borderRadius: BorderRadius.circular(30.0),
	  		shadowColor: Colors.lightBlueAccent.shade100,
	  		elevation: 5.0,
	  		child: MaterialButton(
	  			minWidth: 200.0,
	  			height: 42.0,
	  			onPressed: () {
	  				Navigator.of(context).pushNamed(HomePage.tag);
	  			},
	  			color: Colors.lightBlueAccent,
	  			child: Text(
	  				'Log In',
	  				style: TextStyle(
	  					color: Colors.white,
	  				),
	  			),
	  		),
	  	),
	  );

	  final forgotLabel = FlatButton(
	  	child: Text(
	  		'Olvidaste tu password?',
	  		style: TextStyle(color: Colors.black54),
	  	),
	  	onPressed: () {},
	  );

    return Scaffold(
    	backgroundColor: Colors.white,
      body: Center(
        child: ListView(
        	shrinkWrap: true,
        	padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
          	logo,
          	SizedBox(height: 48.0),
          	email,
          	SizedBox(height: 8.0),
          	password,
          	SizedBox(height: 24.0),
          	loginButton,
          	forgotLabel,
          ],
        ),
      ),
    );
  }
}